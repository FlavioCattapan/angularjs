<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html ng-app="app" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
</head>
<body>
   
   <!-- escopo do controller -->
   <div ng-controller="Controller" >
       {{nome}}
       <button type="button" ng-click="funcola('Flávio')" >Click Here</button>
   </div>
   
   
    <script type="text/javascript" src="../resources/js/angular/angular.min.js" ></script>
    <script type="text/javascript" src="../resources/js/basico/module.js" ></script>
    <!-- controllers  -->
    <script type="text/javascript" src="../resources/js/basico/controller/Controller.js" ></script>

</body>
</html>