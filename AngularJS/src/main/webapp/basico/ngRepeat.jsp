<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html ng-app="app">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body ng-controller="Repeat" >

	<div  >
		<ul>
		   <li ng-repeat="nome in nomes"  >{{nome}}</li>
		</ul>
	</div>
	
	<input type="text" id="nomepessoa" />
	<input type="text" id="idadepessoa" />
	
	<button ng-click="adicionaPessoa()" >Adicionar</button>
	
	
	<div  >
		<ul>
		   <li ng-repeat="pessoa in pessoas"  >{{pessoa.nome}} - {{pessoa.idade}}</li>
		</ul>
	</div>

	<script type="text/javascript" src="../resources/js/angular/angular.min.js"></script>
	<script type="text/javascript" src="../resources/js/basico/module.js"></script>
	<script type="text/javascript"
		src="../resources/js/basico/controller/RepeatController.js"></script>

</body>
</html>