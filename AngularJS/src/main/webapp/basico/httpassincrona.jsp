<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html ng-app="app" >
<head>
<meta charset="utf-8"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
</head>
<body>

    <div ng-controller="HttpAssincronaController" >
    
    	<button type="button" ng-click="carregaDados()"  > Carregar os dados</button>
    	<p ng-repeat="pessoa in dados" >
    	{{ pessoa.nome }} / {{ pessoa.idade }}
    	 </p>
    	 
    	 <hr/>
    	 
    	 <input type="text" ng-model="cidade"  />
    	 
    	 <button type="button" ng-click="carregaPrevisao()"  >
    	    Visualizar tempo
    	 </button>
    	 
    
    </div>
   
    <script type="text/javascript" src="../resources/js/angular/angular.min.js" ></script>
    <script type="text/javascript" src="../resources/js/basico/module.js"></script>
    <script type="text/javascript" src="../resources/js/basico/controller/HttpAssincronaController.js"></script>
    
</body>
</html>