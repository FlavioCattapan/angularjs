<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html ng-app ng-init="valor1 = 'Flávio Cattapan'; valor2 = '  Programador Java'; valor3 = ' Ngbing é melhor que chaves duplas evita aparecer {{}} '; conta = { total : 10 , desp : 5  } " >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
</head>
<body>
   
	<h1 ng-bind="valor1 + valor2" ></h1>

	<h2> {{valor3}} </h2> 
	
	<h3> Cálculo com objetos : {{conta.total - conta.desp}}  </h3>     
    
    <h3> Pegando o valor da imput e setando na variável ng-model <input type="number" ng-model="conta.total" /> </h3>
    
    <script type="text/javascript" src="../resources/js/angular/angular.min.js" ></script>

</body>
</html>