<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html  ng-app="Hello" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
</head>
<body ng-controller=""  >
    <div>
      <label>Name:</label>
      <input type="text" ng-model="yourName" placeholder="Enter a name here">
      <input type="text" ng-model="yourName" placeholder="Enter a name here">
      <hr>
      <h1>Hello {{yourName}}!</h1>
      <h1>Hello {{yourName}}!</h1>
    </div>
     <br/>
     <div>
      <button ng-click="incrementar()"  >Clicar</button>
      <label>Você clicou: {{total}} vezes(s)</label>
     </div>
     
     <footer ng-controller="RodapeController" >
           {{copyright}}
     </footer>
    
      <script type="text/javascript" src="../resources/js/angular/angular.min.js" ></script>
      <script  src="../resources/js/basico/main.js"></script>
      <script  src="../resources/js/basico/controller/HelloController.js"></script>
      <script  src="../resources/js/basico/controller/RodapeController.js"></script>

</body>
</html>