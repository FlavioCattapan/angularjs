<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
</head>
<body>

    <h1> Itens de Estudo </h1>
    <div style="float: left;" >
	<h2>B&aacute;sico</h2>
    <ul>
        <li><a href="hello-word.jsp" >Hello Word</a></li>
        <li><a href="ngbind-expressoes.jsp" >ngBinding</a></li>
        <li><a href="modulos.jsp" >Modulos</a></li>
        <li><a href="controller.jsp" >Controller</a></li>
        <li><a href="ngRepeat.jsp" >ngRepeat</a></li>
        <li><a href="form.jsp" >Formulário</a></li>
        <li><a href="servicefactory.jsp" >Service Factory</a></li>
        <li><a href="filter.jsp" >Filter</a></li>
        <li><a href="route.html" >Route</a></li>
        <li><a href="diretivas.jsp" >Diretivas</a></li>
        <li><a href="diretivascustomizadas.jsp" >Diretivas Customizadas</a></li>
        <li><a href="httpassincrona.jsp" >Http assincrona</a></li>
        <li><a href="resource.jsp" >Http Resource</a></li>
    </ul>
    </div>
    <div style="float: left;" >
    <h2>Avan&ccedil;ado</h2>
    <ul>
        <li><a href="../avancado/angularjquery.jsp" >Angular Jquery</a></li>
        <li><a href="../avancado/animation.jsp" >Animation</a></li>
        <li><a href="../avancado/location.jsp" >Location</a></li>
        <li><a href="../avancado/cookies.jsp" > Cookies</a></li>
        <li><a href="../avancado/internacionalizacao.jsp" > Intercionalizacao</a></li>
        <li><a href="../avancado/highlightmask.jsp" > Highlight Mask </a></li>
        <li><a href="../avancado/uiuniqueuivalidate.jsp" > Uiunique Uivalidate </a></li>
        <li><a href="../avancado/maps.jsp" > Maps </a></li>
        <li><a href="../avancado/select.jsp" > Selected </a></li>
        <li><a href="../avancado/bootstrap.jsp" > Bootstrap </a></li>
    </ul>
    </div>
</body>
</html>