<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html ng-app="app" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
</head>

<body  >

	<div ng-controller="ServiceFactory">Controller 1
	   {{pessoa.nome}} - {{pessoa.idade}}
	</div>



	<div ng-controller="ServiceFactory2">Controller 2
		  {{pessoa.nome}} - {{pessoa.idade}}
	</div>

	<script type="text/javascript" src="../resources/js/angular/angular.min.js" ></script>
    <script type="text/javascript" src="../resources/js/basico/module.js"></script>
    <script type="text/javascript" src="../resources/js/basico/service/Service.js"></script>
    <script type="text/javascript" src="../resources/js/basico/controller/ServiceFactoryController.js"></script>
    <script type="text/javascript" src="../resources/js/basico/factory/Factory.js"></script>
    
</body>
</html>