<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html ng-app="app" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
</head>
<body>

    <div ng-controller="FilterController" >
        Outra forma de criar o controller
        <br/>
        {{ 'Flávio Cattapan' | uppercase }} <br/>
        {{ (pessoa | json) | uppercase }} <br/>
 		{{ 'Flávio' | SaudacaoFilter }}   
    </div>
   
    <script type="text/javascript" src="../resources/js/angular/angular.min.js" ></script>
    <script type="text/javascript" src="../resources/js/basico/module.js"></script>
    <script type="text/javascript" src="../resources/js/basico/controller/FilterController.js"></script>
    <script type="text/javascript" src="../resources/js/basico/Filter/Filter.js"></script>
    
</body>
</html>