<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html ng-app="app" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
</head>
<body>


	<div ng-controller="ResourceController" >
	
		<button type="button" ng-click="carregaUsuario()" >Carregar Usuario</button>
		<button type="button" ng-click="listarUsuarios()" >Listar Usuarios</button>
		
		<p ng-repeat="pessoa in dados" >
    	{{ pessoa.username }} / {{ pessoa.password }}
    	 </p>
			
	
	</div>

   
    <script type="text/javascript" src="../resources/js/angular/angular.min.js" ></script>
    <script type="text/javascript" src="../resources/js/angular/angular-resource.min.js" ></script>
    <script type="text/javascript" src="../resources/js/basico/controller/ResourceController.js"></script>
    
</body>
</html>