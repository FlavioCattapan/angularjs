<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html ng-app="app" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<style type="text/css">
[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak
	{
	display: none !important;
}

.classNgClass {
	font-size: 50px;
	color: red;
}
</style>

    <script type="text/javascript" src="../resources/js/angular/angular.min.js" ></script>
</head>
<body>

	<div  ng-controller="DiretivasController" >
	
	<h1>ng-Show | ng-Hide</h1>
	
	<div ng-show="mostar"  >Este bloco é exibico quando mostrar for true</div>
	<div ng-hide="mostar"  >Este bloco é exibico quando mostrar for false</div>
	
	<input type="checkbox" ng-model="mostar" > Selecionado mostar = true </input>
	
	<h1>Elemento A</h1>
	<a href="" >Abrir nova página - </a>

		<h1>Binding html</h1>
		<p ng-bind-html="variavelHtml"></p>

		<button type="button" ng-click="minhaClass='classNgClass'">
			Add Class</button>

		<button type="button" ng-click="minhaClass=''">Remove Class</button>

		<div ng-class="minhaClass">DIV para testes</div>

		<h1>ngCloak</h1>
		<p>Previne que variáveis do tipo {{ }} sejam exibidas no browser</p>

		<p ng-cloak class="ng-cloak">{{ variavelCloak }}</p>

		<h1>ngSrc</h1>

		<img ng-src="../resources/imgs/{{minhaFoto}}"  >

		<button type="button" ng-click="minhaFoto='foto1.jpg'">
			Foto1</button>

		<button type="button" ng-click="minhaFoto='foto2.jpg'">
			foto2</button>

	</div>


   
    <script type="text/javascript" src="../resources/js/angular/angular-sanitize.min.js" ></script>
    <script type="text/javascript" src="../resources/js/basico/controller/DiretivasController.js"></script>
    
</body>
</html>