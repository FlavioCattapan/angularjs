<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html ng-app="app" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
<base href="/" >
</head>
<body>

   <div ng-controller="RouteController" >
   		<ul>
   			<li> <a href="/"   ></a> Inicial </li>
   		 	<li> <a href="pagina1"  ></a> Página 1  </li>
   			<li> <a href="pagina2/1" ></a> Página 2 </li>
   		</ul>

		<p>Cabeçalho da Página  </p>  
		
		<div ng-view >
		
		</div> 
   
   		<p> Rodapé da página </p>
   
   </div>



   
    <script type="text/javascript" src="../resources/js/angular/angular.min.js" ></script>
    <script type="text/javascript" src="../resources/js/angular/angular-route.min.js" ></script>
    <script type="text/javascript" src="../resources/js/basico/controller/RouteController.js"></script>
    
</body>
</html>