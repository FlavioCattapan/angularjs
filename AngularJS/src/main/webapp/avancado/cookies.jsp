<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html ng-app="app" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
</head>
<body>

	<div ng-controller="CookiesController" >
		<button type="button" ng-click="criarCookie()" >Criar cookie</button>
		<button type="button" ng-click="getValorCookie()"  >Get valor cookie</button>
		<button type="button" ng-click="removeCookie()"  >Remove cookie</button>
  	</div>

	<div ng-controller="CookiesController2" >
		<button type="button" ng-click="getValorCookie2()"  >Remove cookie</button>
  	</div>

   
    <script type="text/javascript" src="../resources/js/angular/angular.min.js" ></script>
    <script type="text/javascript" src="../resources/js/angular/angular-cookies.min.js" ></script>
    <script type="text/javascript" src="../resources/js/avancado/controller/CookiesController.js"></script>
    
</body>
</html>