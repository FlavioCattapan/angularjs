<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html ng-app="app" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
</head>
<body>

	<div ng-controller="InternacionalizacaoController" >
	
		<h1 translate="TITULO" ></h1>
		<p translate="PARAGRAFO1" ></p>
		
		<button type="button" ng-click="setaIdioma('pt-br')" > Portugues</button>
		<button type="button" ng-click="setaIdioma('en')" > Ingles</button>
  	
  	</div>

   
    <script type="text/javascript" src="../resources/js/angular/angular.min.js" ></script>
    <script type="text/javascript" src="../resources/js/angular/angular-translate.min.js" ></script>
    <script type="text/javascript" src="../resources/js/avancado/controller/InternacionalizacaoController.js"></script>
    
</body>
</html>