<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html ng-app="app" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
</head>
<body>

	<div ng-controller="UiuniqueUivalidateController" ng-init="campo=''" >
	
	    <h1> ui.unique </h1>
	
		<button  type="button" ng-click="campo=''" >Todos</button>
		<button  type="button" ng-click="campo='nome'"  >Nome</button>
		<button  type="button" ng-click="campo=email" >Email</button>
	
		<p ng-repeat="usr in usuarios | unique:campo"  >
		   {{usr.nome }} / {{usr.email}}
		</p>
		
		<h1> ui.validade</h1>
  	
  	    <form name="formulario" >
  	    
  	    	<input type="text" ng-model="senha1"  name="senha1" required placeholder="Senha" >
  	    	<input type="text"  ui-validate="'validar($value)'"  ui-validate-watch="'senha1'"  ng-model="senha2" name="senha2" required placeholder="Confirmar senha" >

			<button  type="submit" ng-disabled="!formulario.$valid" > Enviar </button>
			
			<div ng-show="!formulario.$valid" >Preencha todos os campos</div>
			  	    
  	    </form>
  	
  	</div>

   
    <script type="text/javascript" src="../resources/js/angular/angular.min.js" ></script>
    <script type="text/javascript" src="../resources/js/angular/ui-utils.min.js" ></script>
    <script type="text/javascript" src="../resources/js/angular/angular-sanitize.min.js" ></script>
    <script type="text/javascript" src="../resources/js/avancado/controller/UiuniqueUivalidateController.js"></script>
    
</body>
</html>