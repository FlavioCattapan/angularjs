<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html ng-app="app" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
</head>
<body>

  	<div ng-controller="AngularJqueryController" >
  	
  		<span  tooltip title="Integrando com diretivas"> Integrando Jquey e Angular com diretivas nem sempre funciona</span>
  	
  	</div>
   
    <script src="../resources/js/jquery/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="../resources/js/angular/angular.min.js" ></script>
    <script type="text/javascript" src="../resources/js/angular/tooltipsy.min.js" ></script>
    <script type="text/javascript" src="../resources/js/avancado/module/module.js"></script>
    <script type="text/javascript" src="../resources/js/avancado/controller/AngularJqueryController.js"></script>
    
</body>
</html>