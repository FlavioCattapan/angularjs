<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html ng-app="app" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="../resources/css/select.min.css"  >
</head>
<body>

	<div ng-controller="SelectController"  >

		<div class="container">
			<div class="row">
				<div class="col-xs-12" >
				
				    <h1> ui.select</h1>
					<ui-select ng-model="paisselecionado.selected" theme="bootstrap">

					<ui-select-match placeholder="Digite o nome :"></ui-select-match>

					<ui-select-choices
						repeat="pais in paises | filter : $select.search">
					{{ pais.name}} // {{pais.code}} </ui-select-choices> </ui-select>

					<h3>Selecionou {{ paisselecionado.selected.name }}</h3>
				</div>
			</div>
		</div>
	</div>

   
    <script type="text/javascript" src="../resources/js/angular/angular.min.js" ></script>
    <script type="text/javascript" src="../resources/js/angular/select.min.js" ></script>
    <script type="text/javascript" src="../resources/js/avancado/controller/SelectController.js"></script>
    
</body>
</html>