<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html ng-app="app" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
<link rel="stylesheet" href="../resources/css/bootstrap.min.css">
</head>
<body>

	<div ng-controller="BootstrapController" >
	
		<div class="container" >
			<div class="row" >
				<div class="col-xs-12" >
					<h1>Accordion</h1>
					
					<accordion>
						<accordion-group heading="Titulo do accordion" >
							<p>Texto do accordion </p> 
						 </accordion-group>
						 
						 <accordion-group heading="Titulo do accordion" >
							<p>Texto do accordion </p> 
						 </accordion-group>
						
						 <accordion-group ng-repeat="g in grupos"  heading="{{g.heading}}" >
							<div class="alert alert-success" >
									{{g.content}}
							</div>
						 </accordion-group>
					</accordion>
				
				</div>
			</div>
		</div>
  	
  	</div>


   
    <script type="text/javascript" src="../resources/js/angular/angular.min.js" ></script>
    <script type="text/javascript" src="../resources/js/angular/ui-bootstrap-tpls-0.13.0.min.js" ></script>
    <script type="text/javascript" src="../resources/js/avancado/controller/BootstrapController.js"></script>
    
</body>
</html>