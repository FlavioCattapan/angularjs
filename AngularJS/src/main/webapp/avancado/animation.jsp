<!doctype html '>
<!-- Aplica o angular em toda a tag html podemos utilizar em tags isoladas -->
<html ng-app="app">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style type="text/css">
.esconder {
	padding: 10px;
	border: 1px solid black;
	background: white;
}

.esconder {
	-moz-transition: all linear 1s;
	-webkit-transition: all linear 1s;
	-o-transition: all linear 1s;
	transition: all linear 1s;
}

.esconder.ng-hide {
	opacity: 0;
}

.esconder.ng-show {
	opacity: 1;
}
/* ng-enter ng-enter-active ng-leave ng-move */
.repetir-lista {
	border: 1px solid black;
	padding: 10px;
	margin-bottom: 5px;
}

.repetir-lista.ng-enter {
	-moz-transition: 1s linear all;
	-webkit-transition: 1s linear all;
	-o-transition: 1s linear all;
	transition: 1s linear all;
	opacity: 0;
	background: green;
}

.repetir-lista.ng-enter-active {
	opacity: 1;
	background: white;
}
</style>
</head>
<body>

	<div ng-controller="AnimationController"
		ng-init="mostra=true;itemadd=0">

		<input type="checkbox" ng-model="mostra">

		<div class="esconder" ng-show="mostra">Animandooooo ...</div>

		<hr />

		<input type="text" ng-model="itemadd">

		<button type="button" ng-click="adicionaItem()">Adiciona Item</button>

		<div class="repetir-lista" ng-repeat="item in itens">{{item}}</div>

	</div>


	<script type="text/javascript"
		src="../resources/js/angular/angular.min.js"></script>
	<script type="text/javascript"
		src="../resources/js/angular/angular-animate.min.js"></script>
	<script type="text/javascript"
		src="../resources/js/avancado/controller/AnimationController.js"></script>

</body>
</html>