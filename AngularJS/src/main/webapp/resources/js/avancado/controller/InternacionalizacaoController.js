var app = angular.module('app', ['pascalprecht.translate']);

app.config(function($translateProvider){
	
	$translateProvider.translations('pt-br',{
		TITULO : "Seja bem vindo",
		PARAGRAFO1 : "Essa minha app"
	});
	
	$translateProvider.translations('en',{
		TITULO : "Welcome",
		PARAGRAFO1 : "This a Hello Word Page"
	});
	
	$translateProvider.preferredLanguage('en');
	
});

app.controller('InternacionalizacaoController', function($scope,$translate) {

	
	$scope.setaIdioma = function (sigla){
		
		$translate.use(sigla);
		
	}
	
	
	

});
