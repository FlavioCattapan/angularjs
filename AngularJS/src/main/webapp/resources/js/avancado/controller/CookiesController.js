var app = angular.module("app", ['ngCookies']);


app.controller('CookiesController', function($scope, $cookieStore) {

	$cookieStore.put("nome","RichNet");
	
});

app.controller('CookiesController', function($scope, $cookieStore) {

	 
	$scope.criarCookie = function() {
		$cookieStore.put("meusDados", {"nome" : "Flavio", "idade" : 31});
	}
	
	$scope.getValorCookie = function() {
	   console.log($cookieStore.get("meusDados"));
	}

	$scope.removeCookie = function() {
		$cookieStore.remove("meusDados");
	}
	
});


app.controller('CookiesController2', function($scope, $cookieStore) {

	 
	$scope.getValorCookie2 = function() {
	   console.log($cookieStore.get("meusDados"));
	}

	
});