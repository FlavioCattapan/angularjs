app.factory('Pessoa', function() {
	
	console.log("Criou a Factory");
	
	var Pessoa = function() {
		this.nome = "Flávio",
		this.idade = 31,
		this.cumprimentar = function(){
			return "Olá "+this.nome;
		};
	};
	return Pessoa;
});
    
