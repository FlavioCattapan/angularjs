
app.controller('ServiceFactory', ['$scope', 'operacoes', 'Pessoa',  function($scope, operacoes, Pessoa){
    
	$scope.pessoa = new Pessoa();
	
	console.log("Controller service factory");
    
    console.log(operacoes.somar(10, 10));
    
}]);

app.controller('ServiceFactory2', ['$scope', 'operacoes', 'Pessoa' ,function($scope, operacoes, Pessoa){
    
	$scope.pessoa = new Pessoa();
	$scope.pessoa.nome = 'Ricardo';
	
    console.log("Controller service factory 2");
    
    console.log(operacoes.subtrair(10, 10));
    
}]);