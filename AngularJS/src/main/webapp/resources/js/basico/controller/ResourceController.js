var app = angular.module('app', ['ngResource']);

app.factory('Usuarios', ['$resource', function($resource){

    return $resource('http://localhost:8080/SpringMVC4/views/getUsuarios?idUsuario=1');
    
}]);

app.controller('ResourceController', function($scope, Usuarios, $http) {

		$scope.dados = [];
	
	    $scope.carregaUsuario = function() {
			$scope.dados = Usuarios.get();
		}
	    
	    $scope.listarUsuarios = function() {

			$http.get('http://localhost:8080/SpringMVC4/views/getUsuarios?idUsuario=1').success(function(data) {
				console.log(data);
				$scope.dados = data;
			}).error(function() {
				alert('Não foi possivel carregar o arquivo')
			})

		};
	
});
