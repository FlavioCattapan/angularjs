app.controller('DiretivasCustomizadasController', function($scope) {


});

app.directive('ngOla', function(){
    return {
        restrict : 'AEC',
        
        scope : {
            ngNome : '@'
        },
        
        template : '<h1>Olá {{ngNome}}!</h1>',
        
        controller : ['$scope', function($scope){
            
            $scope.digaOla = function(nome){
                
                alert('Ola '+nome+'!');
                
            }
            
        }],
        // manipula o DOM executada automaticamente
        link : function(scope, iElement, iAttrs){
            
            console.log(iElement);
            console.log(iAttrs);
            // chamando uma funcaodo controle
            scope.digaOla(iAttrs.ngNome);
            
        }
    }
});
