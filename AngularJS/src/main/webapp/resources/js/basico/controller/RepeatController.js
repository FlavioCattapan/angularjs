app.controller('Repeat', function($scope) {

	$scope.nomes = [ 'Jaison', 'Daniel', 'Tiago' ];

	$scope.pessoas = [];

	$scope.pessoas.push(
			{ nome : 'Flávio', idade : 31, status : false}
	);
	$scope.pessoas.push(
			{ nome : 'Claudio', idade : 36, status : false}
	);
	$scope.pessoas.push(
			{ nome : 'Marcos', idade : 33, status : false}
	);

	console.log($scope.nomes);
	console.log($scope.pessoas);
	
	$scope.adicionaPessoa = function(){
		
		var nome = document.getElementById("nomepessoa");
		
		var idade = document.getElementById("idadepessoa");
		
		$scope.pessoas.push(
				{ nome : nome.value, idade : idade.value, status : false }
		);
		
		nome.value = "";
		idade.value = "";
	};

});
